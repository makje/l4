package com.mak1soft;

/**
 * Created by mak1 on 2017-06-08.
 */
public class Date {
    private String date1;
    private String date2;
    private String date3;
    private int date4;

    public String getDate1() {
        return date1;
    }

    public void setDate1(String date1) {
        this.date1 = date1;
    }

    public String getDate2() {
        return date2;
    }

    public void setDate2(String date2) {
        this.date2 = date2;
    }

    public String getDate3() {
        return date3;
    }

    public void setDate3(String date3) {
        this.date3 = date3;
    }

    public int getDate4() {
        return date4;
    }

    public void setDate4(int date4) {
        this.date4 = date4;
    }

}