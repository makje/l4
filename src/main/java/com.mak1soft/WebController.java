package com.mak1soft;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import javax.validation.Valid;

@Controller
public class WebController {

    @Autowired
    Service service;

    @GetMapping("/")
    public String hello(Model model) {
        model.addAttribute("date", new Date());

        return "index";
    }

    @GetMapping("/index")
    public String index(Model model) {
        model.addAttribute("date", new Date());

        return "index";
    }

    @PostMapping("/result")
    public String result(@ModelAttribute @Valid Date date, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors())
            return "error";

        try {
            model.addAttribute("visit", service.computeDays(date));
            model.addAttribute("visit_end_date", service.computeDate(date));
        } catch (IllegalArgumentException e) {
            return "error";
        }

        return "result";
    }

}