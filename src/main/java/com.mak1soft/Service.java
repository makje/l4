package com.mak1soft;

public interface Service {
    int computeDays(Date date);
    String computeDate(Date date);

}
