package com.mak1soft;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.stereotype.Component;

@Component
public class ServiceImpl implements Service {

    public int computeDays(Date date) {
        DateTime dateTime1 = parseDate(date.getDate1());
        DateTime dateTime2 = parseDate(date.getDate2());
        int result = daysBeetwenHelper(dateTime1, dateTime2);

        return ++result;
    }

    public String computeDate(Date date) {
        DateTime dateTime1 = parseDate(date.getDate1());
        DateTime dateTime2 = parseDate(date.getDate2());
        int days = daysBeetwenHelper(dateTime1, dateTime2);
        if (!date.getDate3().isEmpty()) {
            DateTime dateTime3 = parseDate(date.getDate3());
            days = days + daysBeetwenHelper(dateTime2, dateTime3);
        } else
            days = days + date.getDate4();
        DateTime outDate = dateTime1.plusDays(days);

        return outDate.toString().substring(0, outDate.toString().indexOf("T"));
    }

    private int daysBeetwenHelper(DateTime dateStart, DateTime dateEnd) {
        int daysBeetwen = Days.daysBetween(dateStart, dateEnd).getDays();
        if (daysBeetwen >= 0)
            return daysBeetwen;
        else
            throw new IllegalArgumentException("dateEnd > dateStart");
    }

    private DateTime parseDate(String date) {
        DateTimeFormatter dtf = DateTimeFormat.forPattern("MM/dd/yyyy");
        return dtf.parseDateTime(date);
    }

}